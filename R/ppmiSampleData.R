#'@export
ppmiSampleData <- function(protocol = "PPMI", env = "CLN", verbose = interactive(), clean = TRUE, pools = FALSE) {

  # rm(list = ls())
  # load_all()
  # library(dplyr)
  # library(oncore2)
  #
  # verbose = TRUE
  # env = "CLN"
  # protocol = "PPMI"
  # clean = TRUE

  statuses <- c(
    "New" = "New",
    "Deaccessioned" = "Deaccessioned",
    "Destroyed" = "Disposed",
    "Available" = "In Circulation",
    "Shipped" = "Shipped",
    "Reserved" = "Reserved"
  )

  getSampleDomain =     Vectorize(function(site) {
    if(site %in% c(
      "Attikon University Hospital- Athens",
      "Hospital Clinic de Barcelona",
      "Hosp Universitario Donostia",
      "Imperial College London",
      "Innsbruck University",
      "St. Olavs Hospital",
      "Pitie-Salpetriere Hosp",
      "Paracelsus-Elena Klinik",
      "University of Salerno",
      "Univ of Tubingen"
    )) {
      domain = "BioRep"
    } else if(grepl("Aviv", site)) {
      domain = "Tel Aviv"
    } else {
      domain = "Indiana University"
    }

    return(domain)

  })

  query <- oncore_connect(verbose = verbose, env = env, query_name = paste0(protocol, "_getSampleData")) %>%
    select_fields(BSMS_PCS_SPECIMEN.SPECIMEN_STORAGE_LOCATION, SUBJECT_LAST_NAME, CASE_NO,
                  ACCESSION_DATE_TIME, VISIT, REASON_DESTROYED, SPECIMEN_QUANTITY,
                  BSMS_PCS_SPECIMEN.SPECIMEN_UOM, SPECIMEN_BAR_CODE, STUDY_SITE, ALTERNATE_MRN, PCS_SPECIMEN_ID,
                  SPECIMEN_STATUS, SPECIMEN_TYPE, COLLECTION_GROUP, PARENT_SPECIMEN_ID,
                  COLLECTION_DATE_TIME, GENDER, SPECIMEN_NO, ALIQUOT_CREATION_DATE_TIME, SPECIMEN_REQUEST_ID) %>%
    select_annotations("Storage Container", "Nucleic Acid Derivative", "260/280 Ratio",
                       "Date Frozen", "Non-Conformance Reason (specimen)", "Non-Conformance Detail (specimen)") %>%
    filter_oncore(not(specimen_status("New")), protocol(protocol)) %>%
    execute_query(recode = TRUE) %>%
    filter(!(REASON_DESTROYED %in% c("Not Filled", "Extra Label"))) %>%
    recodeField("SPECIMEN_UOM") %>%
    recodeField("NON.CONFORMANCE_REASON_.SPECI", category = "BSM_SPECIMEN_NON-CONFORMANCE")

  project_ids <- oncore2::query_oncore("SELECT SPECIMEN_REQUEST_ID, REQUEST_ACCOUNT_NO FROM ONCORE.SV_BSM_REQUESTS", env = env)

  samples <- query %>%
    left_join(project_ids, by = "SPECIMEN_REQUEST_ID")

  # Apply Collection Date time to sibling samples
  samples <- samples %>%
    group_by(CASE_NO, SPECIMEN_TYPE) %>%
    mutate(COLLECTION_DATE_TIME = ifelse(is.na(COLLECTION_DATE_TIME), unique(COLLECTION_DATE_TIME[!is.na(COLLECTION_DATE_TIME)])[1], COLLECTION_DATE_TIME)) %>%
    group_by(CASE_NO) %>%
    mutate(COLLECTION_DATE_TIME = ifelse(is.na(COLLECTION_DATE_TIME), unique(COLLECTION_DATE_TIME[!is.na(COLLECTION_DATE_TIME)])[1], COLLECTION_DATE_TIME)) %>%
    ungroup()

  # Look for Visit in the CASE_NO when it is missing from VISIT field
  samples <- samples %>%
    mutate(VISIT = ifelse(is.na(VISIT), regmatches(CASE_NO, regexpr("((U|BL|SC|ST|V|PW|new)[0-9]*)$", CASE_NO)), VISIT))

  # Format Date Fields
  samples <- samples %>%
    mutate(ACCESSION_DATE_TIME = format(as.Date(ACCESSION_DATE_TIME, "%m/%d/%Y"), format = "%Y-%m-%d")) %>%
    mutate(ALIQUOT_CREATION_DATE_TIME = format(as.Date(ALIQUOT_CREATION_DATE_TIME, "%m/%d/%Y"), format = "%Y-%m-%d")) %>%
    mutate(DATE_FROZEN = format(as.Date(DATE_FROZEN, "%m/%d/%Y"), format = "%Y-%m-%d")) %>%
    mutate(COLLECTION_DATE_TIME = format(as.Date(COLLECTION_DATE_TIME, "%m/%d/%Y"), format = "%Y-%m-%d"))

  # Calculate Specimen Type
  samples <- samples %>%
    mutate(SPECIMEN_TYPE = ifelse(SPECIMEN_TYPE == "Nucleic Acids" & !is.na(NUCLEIC_ACID_DERIVATIVE), NUCLEIC_ACID_DERIVATIVE, SPECIMEN_TYPE)) %>%
    mutate(SPECIMEN_TYPE = ifelse(SPECIMEN_TYPE == "Nucleic Acids" & COLLECTION_GROUP %in% c("DNA", "RNA"), COLLECTION_GROUP, SPECIMEN_TYPE)) %>%
    mutate(SPECIMEN_TYPE = ifelse(SPECIMEN_TYPE == "Nucleic Acids" & COLLECTION_GROUP == "BUFFY COAT", "DNA", SPECIMEN_TYPE)) %>%
    mutate(SPECIMEN_TYPE = ifelse(SPECIMEN_TYPE == "Nucleic Acids", NA, SPECIMEN_TYPE)) %>%
    mutate(SPECIMEN_TYPE = ifelse(SPECIMEN_TYPE == "miRNA", "RNA", SPECIMEN_TYPE))

  # Recoding and adding fields to meet LONI requirements
  samples <- samples %>%
    mutate(SPECIMEN_STATUS = statuses[SPECIMEN_STATUS]) %>%
    mutate(DOMAIN = getSampleDomain(STUDY_SITE)) %>%
    mutate(STUDY_ID = "PPMI") %>%
    mutate(VOLUME_RECEIVED = "") %>%
    mutate(VOLUME_RECEIVED_UNITS = "") %>%
    mutate(GENDER = substr(GENDER, 1, 1)) %>%
    mutate(SPECIMEN_QUANTITY = ifelse(is.na(SPECIMEN_QUANTITY) & SPECIMEN_STATUS %in% c("Deaccessioned", "Destroyed"), 0, SPECIMEN_QUANTITY)) %>% # Added because of a bunch of missing quantities...
    mutate(PARENT_ID = SPECIMEN_BAR_CODE[match(PARENT_SPECIMEN_ID, PCS_SPECIMEN_ID)]) %>%
    mutate(QC_STATUS = ifelse(is.na(X260.280_RATIO), "Not Complete", "Complete")) %>%
    mutate(QC_DATE = ifelse(!is.na(ALIQUOT_CREATION_DATE_TIME), ALIQUOT_CREATION_DATE_TIME, DATE_FROZEN)) %>%
    mutate(QC_DATE = ifelse(QC_STATUS == "Complete", QC_DATE, NA))

  if(grepl("POOL", protocol)) {
    samples <- mutate(samples, VISIT = ifelse(is.na(VISIT), "POOL", VISIT))
  }

  # Select and order fields
  samples <- samples %>%
    select(
      ALIAS_ID = SUBJECT_LAST_NAME,
      ACCESSION_DATE = ACCESSION_DATE_TIME,
      CLINICAL_EVENT = VISIT,
      CONTAINER_TYPE = STORAGE_CONTAINER,
      CUSTODIAL_DOMAIN = DOMAIN,
      DISPOSITION = REASON_DESTROYED,
      QUANTITY = SPECIMEN_QUANTITY,
      QUANTITY_UNITS = SPECIMEN_UOM,
      PPMI_PROJECT_ID = REQUEST_ACCOUNT_NO,
      SAMPLEID = SPECIMEN_BAR_CODE,
      SITE_ID = STUDY_SITE,
      SMS_SUBJECT_ID=ALTERNATE_MRN,
      STATUS = SPECIMEN_STATUS,
      STUDYID = STUDY_ID,
      TYPE = SPECIMEN_TYPE,
      VOLUME_RECEIVED = VOLUME_RECEIVED,
      VOLUME_RECEIVED_UNITS = VOLUME_RECEIVED_UNITS,
      PARENT_ID = PARENT_ID,
      COLLECTION_DATE = COLLECTION_DATE_TIME,
      REP_GENDER = GENDER,
      QC = QC_STATUS,
      QC_DATE = QC_DATE,
      NON_CONFORMANCE = NON.CONFORMANCE_REASON_.SPECI,
      NON_CONFORMANCE_DETAIL = NON.CONFORMANCE_DETAIL_.SPECI
    ) %>%
    mutate(TYPE = ifelse(is.na(TYPE) & STATUS == "Shipped" & QUANTITY == "0.5", "DNA", TYPE)) # Temp Fix for DNA shipments

  if(pools) {

    pool_samples <- ppmiSampleData(protocol = "POOL", env = env, clean = clean, verbose = verbose, pools = FALSE) %>%
      filter(grepl(protocol, SMS_SUBJECT_ID))

    samples <- rbind(samples,pool_samples) %>%
      arrange(SAMPLEID)

  }

  if(clean) {
    samples <- checkSampleData(samples)
  }

  return(samples)

}

checkSampleData <- function(samples) {

  problem_desc <- c()
  problems <- data.frame()
  date_regex <- "(^(19|20)[0-9][0-9]-[0-1][0-9]-[0-3][0-9]$)|^$"

  samples <- as.data.frame(samples)

  no_na <- c("CLINICAL_EVENT", "QUANTITY", "ALIAS_ID", "CUSTODIAL_DOMAIN", "SAMPLEID", "ACCESSION_DATE", "COLLECTION_DATE", "SMS_SUBJECT_ID", "TYPE")
  date_cols <- c("ACCESSION_DATE", "COLLECTION_DATE", "QC_DATE")

  # Remove records with unacceptable NAs
  for(field in no_na) {
    if(!(test <- see_if(noNA(samples[field])))) {
      problem_desc <- c(problem_desc, gsub("samples[field]", field, attributes(test)$msg, fixed = TRUE))
      p <- samples %>% filter_(paste0("is.na(`", field, "`)")) %>%
        mutate(PROBLEM = paste0(field, " is NA"))
      problems <- rbind(problems, p)
      #      samples <- samples %>% filter_(paste0("!is.na(`", field, "`)"))
    }
  }

  # Remove malformed dates
  for(field in date_cols) {
    if(!(test <- see_if(all_match(samples[field], date_regex)))) {
      problem_desc <- c(problem_desc, gsub("samples[field]", field, attributes(test)$msg, fixed = TRUE))
      p <- samples %>% filter_(paste0("!grepl('", date_regex, "', `", field, "`)")) %>%
        mutate(PROBLEM = paste0(field, " contains a malformed date"))
      problems <- rbind(problems, p)
      #      samples <- samples %>% filter_(paste0("grepl('", date_regex, "', `", field, "`)"))
    }
  }

  samples <- samples %>%
    filter(!SAMPLEID %in% problems$SAMPLEID)

  if(length(problems) == 0) {
    message("No problems found with dataset.")
  } else {
    message("The following error were detected and removed:\n", paste(problem_desc, collapse = "\n"))
  }

  samples$NON_CONFORMANCE_DETAIL <- gsub(",", "", samples$NON_CONFORMANCE_DETAIL)

  attr(samples, "problem_desc") <- problem_desc
  attr(samples, "problems") <- problems

  return(samples)

}

