#' biosendCatalog
#'
#' Generate a BioSEND-style catalog
#'
#' Use this function to generate a catalog of the type suitable for uploading to the BioSEND API.
#'
#' @param protocolName The protocol to filter on. Used as a grep on the PROTOCOL_NO field.
#' @param verbose Should the function let you know what is going on?
#'
#' @export
biosendCatalog <- function(protocolName = "MMGE-NINDS-BIOSEND", verbose = interactive(), headers = TRUE, QAed = TRUE, fix = FALSE, env = "CLN") {

  ##!devtools::load_all("/var/miniCRAN/mmgeverse/oncoreCatalogs")
  ##!QAed = FALSE
  ##!env = "PRD"

  pdbpIds <- readxl::read_excel("/media/HG-Data/MMGE-NINDS-BIOSEND/Data/PDBP_DMR_Study_Names.xlsx")

#  q <- mmgeMongo::mongo_query()

  query <- oncore_connect(error = "warn", verbose = verbose, env=env) %>%
    select_fields(SPECIMEN_STORAGE_LOCATION, SPECIMEN_NO, SPECIMEN_TYPE, STUDY_SITE,
                  VISIT, PARENT_Specimen_ID, ALTERNATE_MRN, SPECIMEN_QUANTITY, ARM_CODE,
                  SPECIMEN_STATUS, UNIT_OF_MEASURE, PCS_SPECIMEN_ID, SPECIMEN_BAR_CODE,
                  COLLECTION_GROUP, PROTOCOL_NO) %>%
    select_annotations('260/280 Ratio', '260/230 Ratio', 'Concentration', 'Concentration UOM',
                      'RIN Value', 'Clotted', 'RNA QC Instrument', "Hemolysis Grade", "rRNA",
                      'Nucleic Acid Derivative', 'Turbidity Grade', 'Hemoglobin Assay') %>%
    filter_oncore(protocol(protocolName)) %>%
    execute_query(recode = FALSE) %>%
    recodeField("NUCLEIC_ACID_DERIVATIVE") %>%                  # Recode fields encoded in OnCore
    recode_field("COLLECTION_GROUP") %>%
    recodeField("CONCENTRATION_UOM") %>%
    recodeField("TURBIDITY_GRADE") %>%
    recodeField("HEMOLYSIS_GRADE") %>%
    recodeField("RNA_QC_INSTRUMENT") %>%
    mutate(CLOTTED = getClotting(CLOTTED, catalog = "biosend"))

  query <- dplyr::left_join(query, pdbpIds)

  missingPDBPStudyIDs <- query %>%
    select(PROTOCOL_NO, ARM_CODE, STUDY_SITE, PDBPStudyID) %>%
    filter(is.na(PDBPStudyID)) %>%
    distinct()

  if(verbose) {
    message("Query Complete...\nBeginning data transformation...")
  }

  if(fix) {
    query <- fixDataForCatalog(query)
  }

  transfer_data <- query %>%
    filter(!COLLECTION_GROUP %in% c("RNA", "DNA", "BUFFY_COAT")) %>%
    group_by(ALTERNATE_MRN, VISIT, COLLECTION_GROUP) %>%
    summarize(
      HEMOGLOBIN_ASSAY = myUnique(HEMOGLOBIN_ASSAY, summary_function = max, na.rm = TRUE)#,
#      TURBIDITY_GRADE = paste0(unique(TURBIDITY_GRADE[!is.na(TURBIDITY_GRADE)]), collapse = "|"),
#      HEMOLYSIS_GRADE = paste0(unique(HEMOLYSIS_GRADE[!is.na(HEMOLYSIS_GRADE)]), collapse = "|")
    ) %>%
    filter(!grepl("|", HEMOGLOBIN_ASSAY, fixed = TRUE)) # %>%
#    filter(!grepl("|", TURBIDITY_GRADE, fixed = TRUE)) %>%
#    filter(!grepl("|", HEMOLYSIS_GRADE, fixed = TRUE))

  q1 <- query %>%
    filter(!is.na(HEMOGLOBIN_ASSAY))
  q2 <- query %>%
    filter(is.na(HEMOGLOBIN_ASSAY)) %>%
    select(-HEMOGLOBIN_ASSAY) %>%
    left_join(transfer_data)

  query <- rbind(q1, q2)

  # This creates a SAMPLE_ID fields based on SPECIMEN_BAR_CODE of the sample or the sample's parent.
  query$SAMPLE_ID <- gsub("\\..+$?", "", query$SPECIMEN_NO)

  # This transfers the data in the parent specimens 'transfer_column' to all its children, I think.
  transfer_columns <- c("X260.280_RATIO", "RIN_VALUE", "CONCENTRATION", "CONCENTRATION_UOM",
                        "CLOTTED", "RNA_QC_INSTRUMENT", "UNIT_OF_MEASURE")
  for(col in transfer_columns) {
    b <- is.na(query[[col]]) & !is.na(query$PARENT_SPECIMEN_ID)
    query[b, col] <- query[match(query$PARENT_SPECIMEN_ID[b], query$PCS_SPECIMEN_ID), col]
  }

  query <- query %>%
    filter(SPECIMEN_STATUS == "Available") %>%
    filter(!is.na(SPECIMEN_STORAGE_LOCATION)) %>%               # Remove missing Storage Locations
    filter(!is.na(VISIT)) %>%                                   # Remove missing Visits
    filter(!is.na(ALTERNATE_MRN)) %>%                           # Remove missing Subject IDs
    filter(!is.na(SPECIMEN_QUANTITY)) %>%                       # Remove missing quantities
    filter(!is.na(UNIT_OF_MEASURE)) %>%                         # Remove missing units of measure
    filter(!grepl("RIN", SPECIMEN_STORAGE_LOCATION)) %>%        # Remove samples destined for RIN Analysis
    filter(!(SPECIMEN_TYPE == "Nucleic Acids" &
               is.na(NUCLEIC_ACID_DERIVATIVE))) %>%             # Remove Nucleic Acids missing a derivative annotation
    filter(!(SPECIMEN_TYPE == "Nucleic Acids"
             & UNIT_OF_MEASURE != "ug")) %>%                    # Remove Nucleic Acids with UOM other than ug
    filter(!(SPECIMEN_TYPE != "Nucleic Acids"
             & UNIT_OF_MEASURE != "ul")) %>%                    # Remove Non-Nucleic Acids with UOM other than ul
    mutate(SPECIMEN_TYPE = ifelse(SPECIMEN_TYPE == "Nucleic Acids",
                                  NUCLEIC_ACID_DERIVATIVE,
                                  SPECIMEN_TYPE)) %>%           # Change Nucleic Acids to actual type (DNA, RNA, miRNA)
    mutate(DOMAIN = "Indiana University")                       # Calculate Domain based on Storage Location

  # These are transformations requested by DMR
  query <- query %>%
    mutate(VISIT = gsub("M", " months", VISIT)) %>%    # spell out months
    mutate(VISIT = gsub("BL", "Baseline", VISIT)) %>%
    mutate(VISIT = gsub("SC", "Screening", VISIT)) %>%
    mutate(STUDY = gsub("-.*$", "", ARM_CODE)) %>%
    mutate(STUDY = gsub("PDBP", "NINDS", STUDY)) %>%
    mutate(STUDY = ifelse(toupper(STUDY) == "ARM1", gsub(paste0(protocolName, "-"), "", PROTOCOL_NO), STUDY)) %>%
#    mutate(STUDY = gsub("UDALL", "NINDS", STUDY)) %>%
    mutate(HEMOGLOBIN_UNITS = ifelse(is.na(HEMOGLOBIN_ASSAY), NA, "ng/ml")) %>%
    mutate(HEMOGLOBIN_RESULT = ifelse(as.numeric(HEMOGLOBIN_ASSAY) >=200, "Fail", "Pass")) %>%
    mutate(COLLECTION_GROUP = if_else(SPECIMEN_TYPE == "Saliva", "SALIVA", COLLECTION_GROUP)) %>% # Temporary fix until Saliva COLLECTION_GROUP is fixed
    mutate(HEMOGLOBIN_RESULT_DESC = ifelse(HEMOGLOBIN_RESULT == "Fail", "Greater than or equal to 200", ifelse(as.numeric(HEMOGLOBIN_ASSAY) <= 30, "Less than or equal to 30", "Greater than 30 and less than 200")))

  if(verbose) message("Analyzing parent information...")
  parents <- query %>%
    filter(is.na(PARENT_SPECIMEN_ID)) %>%
    filter(ifelse(COLLECTION_GROUP %in% c("PLASMA", "SERUM", "CSF"), SPECIMEN_QUANTITY >= 500, SPECIMEN_QUANTITY != 0)) %>%
    filter(SPECIMEN_STATUS == "Available") %>%
#    mutate(PARENTS = "Y")
    mutate(VA = ifelse(COLLECTION_GROUP %in% c("SERUM", "PLASMA", "CSF"), floor(SPECIMEN_QUANTITY / 200) - 1, NA)) %>%
    group_by(STUDY, PDBPStudyID, DOMAIN, COLLECTION_GROUP, VISIT, ALTERNATE_MRN) %>%
    summarize(
      PARENT_SAMPLE_ID = first(SAMPLE_ID),
      PARENT_COUNT = n(),
      PARENT_QUANTITY = max(SPECIMEN_QUANTITY, na.rm = TRUE),
      PARENTS = "Y",
      VIRTUAL_ALIQUOTS = sum(VA)
    ) %>%
    ungroup() %>%
    mutate(COLLECTION_GROUP = ifelse(COLLECTION_GROUP == "BUFFY COAT", "DNA", COLLECTION_GROUP)) %>%
    left_join(biosend_sampleTypes, by = c(COLLECTION_GROUP = "old")) %>%
    rename("TYPE" = new)

  if(verbose) message("Analyzing aliquot information...")
  aliquots <- query %>%
    filter(!is.na(PARENT_SPECIMEN_ID)) %>%
    mutate(ORIGINAL_PARENT = getOriginalParent(SPECIMEN_NO))
  if(nrow(aliquots) > 0) {
    aliquots <- aliquots %>%
      group_by(ORIGINAL_PARENT, STUDY, PDBPStudyID, SPECIMEN_QUANTITY, COLLECTION_GROUP, CONCENTRATION, CONCENTRATION_UOM, RIN_VALUE, X260.280_RATIO, X260.230_RATIO, RRNA) %>%
      summarize(
          SAMPLE_ID = first(SAMPLE_ID),
          DOMAIN = unique(DOMAIN),
          TYPE = unique(SPECIMEN_TYPE),
          VISIT = unique(VISIT),
          ALTERNATE_MRN = unique(ALTERNATE_MRN),
          UNIT_OF_MEASURE = unique(UNIT_OF_MEASURE),
          COUNT = n(),
          RNA_QC_INSTRUMENT = myUnique(RNA_QC_INSTRUMENT),
          CLOTTED = myUnique(CLOTTED),
          HEMOGLOBIN_ASSAY = myUnique(HEMOGLOBIN_ASSAY, summary_function = max, na.rm = TRUE),
          HEMOLYSIS_SCALE = myUnique(HEMOLYSIS_GRADE),
          TURBIDITY_GRADE = myMode(TURBIDITY_GRADE),
          HEMOGLOBIN_UNITS = unique(HEMOGLOBIN_UNITS),
          HEMOGLOBIN_RESULT = myUnique(HEMOGLOBIN_RESULT),
          HEMOGLOBIN_RESULT_DESC = myUnique(HEMOGLOBIN_RESULT_DESC)
        ) %>%
      ungroup() %>%
      filter(removeResiduals(TYPE, SPECIMEN_QUANTITY, catalog = "biosend"))
  }

  if(verbose) message("Building DNA catalog...")
  dna_catalog <- aliquots %>%
    filter(TYPE == "DNA") %>%
    filter(SPECIMEN_QUANTITY > 0) %>%
    filter(CONCENTRATION_UOM %in% c("ng/ul", "ug/ml", "ug/ul", "mg/ml", "ng/ml")) %>% # Cleaning up bad units
    mutate(CONCENTRATION = getDNAConcentration(CONCENTRATION, CONCENTRATION_UOM)) %>%
    group_by(ORIGINAL_PARENT, STUDY, DOMAIN, VISIT, PDBPStudyID, ALTERNATE_MRN, CONCENTRATION,
             CONCENTRATION_UOM, X260.280_RATIO, X260.230_RATIO) %>%
    summarize(
      SAMPLE_ID = SAMPLE_ID[!is.na(SAMPLE_ID) & SAMPLE_ID != ""][1],
      MASS = round(sum(SPECIMEN_QUANTITY* COUNT), 0),
      COUNT = sum(COUNT),
      QC_STATUS = ifelse(is.na(unique(X260.280_RATIO)), "", "QC complete")
    ) %>%
    mutate(COUNT = ifelse(is.na(COUNT), 0, COUNT)) %>%
    mutate(DNA_COUNT = ifelse(COUNT >= 1, 1, 0)) %>%
    left_join(parents %>% filter(COLLECTION_GROUP %in% c("BUFFY COAT", "DNA")) %>% select(-PARENT_SAMPLE_ID)) %>%
    mutate(TYPE = "DNA") %>%
    mutate(DNA_PARENTS = ifelse(!is.na(PARENTS), "Y", "N")) %>%
    ungroup() %>%
    select(SAMPLE_ID, STUDY, DOMAIN, TYPE, VISIT, PDBPStudyID, ALTERNATE_MRN,
           "COUNT" = MASS, "PARENTS" = DNA_PARENTS, MASS = DNA_COUNT, CONCENTRATION,
           CONCENTRATION_UOM, X260.280_RATIO, QC_STATUS)

  if(QAed) {
    dna_catalog <- dna_catalog %>%
      filter(!is.na(CONCENTRATION) & !is.na(X260.280_RATIO))
  }

  if(verbose) message("Building RNA catalog...")
  rna_catalog <- aliquots %>%
    filter(TYPE %in% c("miRNA", "RNA")) %>%
    mutate(SPECIMEN_QUANTITY = floor(SPECIMEN_QUANTITY)) %>%
    filter(CONCENTRATION_UOM %in% c("ng/ul", "ug/ml", "ug/ul", "mg/ml", "ng/ml") | is.na(CONCENTRATION_UOM)) %>% # Cleaning up bad units
    mutate(CONCENTRATION = getRNAConcentration(CONCENTRATION, CONCENTRATION_UOM)) %>%
    group_by(ORIGINAL_PARENT) %>%
    summarize(
      COUNT = sum(SPECIMEN_QUANTITY * COUNT),
      MASS = 1,
      QC_CLASS = getRNAClass(unique(RIN_VALUE), unique(X260.280_RATIO), type = "biosend"),
      QC_ROBOT = myUnique(unique(RNA_QC_INSTRUMENT)),
      QC_STATUS = getRNAStatus(unique(RIN_VALUE), unique(X260.280_RATIO))
    ) %>%
    left_join(aliquots %>% select(-SPECIMEN_QUANTITY, -COUNT), by = "ORIGINAL_PARENT") %>%
    distinct() %>%
    left_join(parents %>% filter(COLLECTION_GROUP == "RNA") %>% select(-PARENT_SAMPLE_ID)) %>%
    mutate(TYPE = "RNA") %>%
    mutate(RNA_PARENTS = ifelse(is.na(PARENTS), "N", "Y")) %>%
    ungroup() %>%
    select(SAMPLE_ID = ORIGINAL_PARENT, STUDY, DOMAIN, TYPE, VISIT, PDBPStudyID, ALTERNATE_MRN, COUNT, "PARENTS" = RNA_PARENTS, MASS, CONCENTRATION, CONCENTRATION_UOM,
           X260.280_RATIO, X260.230_RATIO, rRatio = RRNA, RIN_VALUE, QC_CLASS, QC_STATUS)

  if(QAed) {
    rna_catalog <- rna_catalog %>%
      filter(!is.na(RIN_VALUE) & !is.na(CONCENTRATION))
  }

  if(verbose) message("Building other specimens catalog...")
  other_catalog <- aliquots %>%
    filter(TYPE %in% c("Serum", "Plasma", "Cerebrospinal Fluid")) %>%
    group_by(ORIGINAL_PARENT, STUDY, DOMAIN, COLLECTION_GROUP, VISIT, PDBPStudyID, ALTERNATE_MRN, SPECIMEN_QUANTITY) %>%
    summarize(
      SAMPLE_ID = paste0(SAMPLE_ID[!is.na(SAMPLE_ID) & SAMPLE_ID != ""][1], "_", SPECIMEN_QUANTITY),
      COUNT = sum(COUNT),
      HEMOGLOBIN_ASSAY = myUnique(HEMOGLOBIN_ASSAY, summary_function = max, na.rm = TRUE),
      CLOTTED = myUnique(CLOTTED),
      TURBIDITY_GRADE = myMode(TURBIDITY_GRADE),
      TYPE = unique(TYPE),
      HEMOGLOBIN_UNITS = myUnique(HEMOGLOBIN_UNITS),
      HEMOGLOBIN_RESULT = myUnique(HEMOGLOBIN_RESULT),
      HEMOGLOBIN_RESULT_DESC = myUnique(HEMOGLOBIN_RESULT_DESC),
      HEMOLYSIS_SCALE = myUnique(HEMOLYSIS_SCALE)
    ) %>%
    full_join(parents %>%
                ungroup() %>%
                filter((COLLECTION_GROUP %in% c("SERUM", "PLASMA", "CSF"))) %>%
                select(-PARENT_SAMPLE_ID, -DOMAIN),
      by = c("TYPE", "STUDY", "VISIT", "ALTERNATE_MRN", "PDBPStudyID")) %>%
    mutate(OTHER_PARENTS = ifelse(is.na(PARENTS), "N", "Y")) %>%
    mutate(QUANTITY_UNITS = "ul") %>%
    mutate(CLOTTED = ifelse(TYPE == "Cerebrospinal Fluid", "", CLOTTED)) %>%
    mutate(TYPE = ifelse(TYPE == "Cerebrospinal Fluid", "CSF", TYPE)) %>%
#    mutate(COUNT = ifelse(is.na(VIRTUAL_ALIQUOTS), COUNT, COUNT + unique(VIRTUAL_ALIQUOTS))) %>%
    ungroup() %>%
    select(SAMPLE_ID, STUDY, DOMAIN, TYPE, VISIT, PDBPStudyID, ALTERNATE_MRN, SPECIMEN_QUANTITY, QUANTITY_UNITS, COUNT,
           PARENTS = OTHER_PARENTS, HEMOGLOBIN_ASSAY, HEMOGLOBIN_UNITS, HEMOGLOBIN_RESULT, HEMOGLOBIN_RESULT_DESC,
           HEMOLYSIS_SCALE, CLOTTED, TURBIDITY_GRADE, VIRTUAL_ALIQUOTS) %>%
    group_by(TYPE, VISIT, PDBPStudyID, ALTERNATE_MRN, SPECIMEN_QUANTITY, HEMOGLOBIN_ASSAY, HEMOGLOBIN_UNITS, HEMOGLOBIN_RESULT, HEMOLYSIS_SCALE, CLOTTED, TURBIDITY_GRADE) %>%
    mutate(COUNT = sum(COUNT)) %>%
    mutate(SAMPLE_ID = first(SAMPLE_ID)) %>%
    mutate(VIRTUAL_ALIQUOTS = ifelse(is.na(VIRTUAL_ALIQUOTS), 0, VIRTUAL_ALIQUOTS)) %>%
    mutate(COUNT = ifelse(SPECIMEN_QUANTITY == 200, COUNT + VIRTUAL_ALIQUOTS, COUNT)) %>%
    ungroup() %>%
    select(-VIRTUAL_ALIQUOTS) %>%
    distinct()

  other_parent_only <- other_catalog %>%
    ungroup() %>%
    filter(is.na(SAMPLE_ID)) %>%
    select(-DOMAIN) %>%
    mutate(COLLECTION_GROUP = toupper(TYPE)) %>%
    left_join(parents %>% ungroup() %>% select(STUDY, DOMAIN, PARENT_SAMPLE_ID, PDBPStudyID,
                                               COLLECTION_GROUP, VISIT, ALTERNATE_MRN,
                                               VIRTUAL_ALIQUOTS),
              by = c("COLLECTION_GROUP", "VISIT", "STUDY", "ALTERNATE_MRN", "PDBPStudyID")) %>%
    mutate(COUNT = VIRTUAL_ALIQUOTS) %>%
    mutate(SAMPLE_ID = PARENT_SAMPLE_ID) %>%
    select(-VIRTUAL_ALIQUOTS, -COLLECTION_GROUP, - PARENT_SAMPLE_ID) %>%
    mutate(SPECIMEN_QUANTITY = 200) %>%
    select(SAMPLE_ID, STUDY, DOMAIN, TYPE, VISIT, PDBPStudyID, ALTERNATE_MRN, SPECIMEN_QUANTITY, QUANTITY_UNITS, COUNT,
           PARENTS, HEMOGLOBIN_ASSAY, HEMOGLOBIN_UNITS, HEMOGLOBIN_RESULT, HEMOGLOBIN_RESULT_DESC,
           HEMOLYSIS_SCALE, CLOTTED, TURBIDITY_GRADE) %>%
    ungroup()

  other_catalog <- other_catalog %>%
    filter(!is.na(SAMPLE_ID)) %>%
    rbind(other_parent_only) %>%
    distinct(SAMPLE_ID, .keep_all = TRUE)

  # if(QAed) {
  #   other_catalog <- other_catalog %>%
  #     filter(HEMOGLOBIN_ASSAY != "")
  # }

  if(verbose) message("Building urine specimens catalog...")
  urine_catalog <- aliquots %>%
    filter(COLLECTION_GROUP == "URINE" & TYPE == "Urine") %>%
    group_by(ORIGINAL_PARENT, STUDY, DOMAIN, COLLECTION_GROUP, VISIT, PDBPStudyID, ALTERNATE_MRN, SPECIMEN_QUANTITY) %>%
    summarize(
      SAMPLE_ID = paste0(SAMPLE_ID[!is.na(SAMPLE_ID) & SAMPLE_ID != ""][1], "_", SPECIMEN_QUANTITY),
      COUNT = sum(COUNT),
      HEMOGLOBIN_ASSAY = myUnique(HEMOGLOBIN_ASSAY, summary_function = max, na.rm = TRUE),
      CLOTTED = myUnique(CLOTTED),
      TURBIDITY_GRADE = myMode(TURBIDITY_GRADE),
      TYPE = unique(TYPE),
      HEMOGLOBIN_UNITS = myUnique(HEMOGLOBIN_UNITS),
      HEMOGLOBIN_RESULT = myUnique(HEMOGLOBIN_RESULT),
      HEMOGLOBIN_RESULT_DESC = myUnique(HEMOGLOBIN_RESULT_DESC),
      HEMOLYSIS_SCALE = myUnique(HEMOLYSIS_SCALE)
    ) %>%
    full_join(parents %>%
                filter(COLLECTION_GROUP == "URINE")) %>%
    ungroup() %>%
    mutate(SAMPLE_ID = ifelse(is.na(SAMPLE_ID), PARENT_SAMPLE_ID, SAMPLE_ID)) %>%
    mutate(SPECIMEN_QUANTITY = ifelse(is.na(SPECIMEN_QUANTITY), PARENT_QUANTITY, SPECIMEN_QUANTITY)) %>%
    mutate(COUNT = ifelse(is.na(COUNT), PARENT_COUNT, COUNT)) %>%
    mutate(TYPE = "Urine") %>%
    mutate(BLOOD_PARENTS = ifelse(is.na(PARENTS), "N", "Y")) %>%
    mutate(SPECIMEN_QUANTITY = ifelse(SPECIMEN_QUANTITY == "", 1000, SPECIMEN_QUANTITY)) %>%
    mutate(COUNT = ifelse(is.na(COUNT), 0, COUNT)) %>%
    mutate(QUANTITY_UNITS = "ul") %>%
    ungroup() %>%
    select(SAMPLE_ID, STUDY, DOMAIN, TYPE, VISIT, PDBPStudyID, ALTERNATE_MRN, SPECIMEN_QUANTITY, QUANTITY_UNITS, COUNT,
           PARENTS = BLOOD_PARENTS, HEMOGLOBIN_ASSAY, HEMOGLOBIN_UNITS, HEMOGLOBIN_RESULT, HEMOGLOBIN_RESULT_DESC,
           HEMOLYSIS_SCALE, CLOTTED, TURBIDITY_GRADE)

  if(verbose) message("Building saliva specimens catalog...")
  saliva_catalog <- aliquots %>%
    filter(COLLECTION_GROUP == "SALIVA" & TYPE == "Saliva") %>%
    group_by(ORIGINAL_PARENT, STUDY, DOMAIN, COLLECTION_GROUP, VISIT, PDBPStudyID, ALTERNATE_MRN, SPECIMEN_QUANTITY) %>%
    summarize(
      SAMPLE_ID = paste0(SAMPLE_ID[!is.na(SAMPLE_ID) & SAMPLE_ID != ""][1], "_", SPECIMEN_QUANTITY),
      COUNT = sum(COUNT),
      HEMOGLOBIN_ASSAY = myUnique(HEMOGLOBIN_ASSAY, summary_function = max, na.rm = TRUE),
      CLOTTED = myUnique(CLOTTED),
      TURBIDITY_GRADE = myMode(TURBIDITY_GRADE),
      TYPE = unique(TYPE),
      HEMOGLOBIN_UNITS = myUnique(HEMOGLOBIN_UNITS),
      HEMOGLOBIN_RESULT = myUnique(HEMOGLOBIN_RESULT),
      HEMOGLOBIN_RESULT_DESC = myUnique(HEMOGLOBIN_RESULT_DESC),
      HEMOLYSIS_SCALE = myUnique(HEMOLYSIS_SCALE)
    ) %>%
    full_join(parents %>%
                filter(COLLECTION_GROUP == "SALIVA")) %>%
    ungroup() %>%
    mutate(SAMPLE_ID = ifelse(is.na(SAMPLE_ID), PARENT_SAMPLE_ID, SAMPLE_ID)) %>%
    mutate(SPECIMEN_QUANTITY = ifelse(is.na(SPECIMEN_QUANTITY), PARENT_QUANTITY, SPECIMEN_QUANTITY)) %>%
    mutate(COUNT = ifelse(is.na(COUNT), PARENT_COUNT, COUNT)) %>%
    mutate(TYPE = "Saliva") %>%
    mutate(BLOOD_PARENTS = ifelse(is.na(PARENTS), "N", "Y")) %>%
    mutate(SPECIMEN_QUANTITY = ifelse(SPECIMEN_QUANTITY == "", 1000, SPECIMEN_QUANTITY)) %>%
    mutate(COUNT = ifelse(is.na(COUNT), 0, COUNT)) %>%
    mutate(QUANTITY_UNITS = "ul") %>%
    ungroup() %>%
    select(SAMPLE_ID, STUDY, DOMAIN, TYPE, VISIT, PDBPStudyID, ALTERNATE_MRN, SPECIMEN_QUANTITY, QUANTITY_UNITS, COUNT,
           PARENTS = BLOOD_PARENTS, HEMOGLOBIN_ASSAY, HEMOGLOBIN_UNITS, HEMOGLOBIN_RESULT, HEMOGLOBIN_RESULT_DESC,
           HEMOLYSIS_SCALE, CLOTTED, TURBIDITY_GRADE)


  if(verbose) message("Building blood specimens catalog...")
  blood_catalog <- aliquots %>%
    filter(COLLECTION_GROUP == "WBLD" & TYPE == "Whole Blood") %>%
    group_by(ORIGINAL_PARENT, STUDY, DOMAIN, COLLECTION_GROUP, VISIT, PDBPStudyID, ALTERNATE_MRN, SPECIMEN_QUANTITY) %>%
    summarize(
      SAMPLE_ID = paste0(SAMPLE_ID[!is.na(SAMPLE_ID) & SAMPLE_ID != ""][1], "_", SPECIMEN_QUANTITY),
      COUNT = sum(COUNT),
      HEMOGLOBIN_ASSAY = myUnique(HEMOGLOBIN_ASSAY, summary_function = max, na.rm = TRUE),
      CLOTTED = myUnique(CLOTTED),
      TURBIDITY_GRADE = myMode(TURBIDITY_GRADE),
      TYPE = unique(TYPE),
      HEMOGLOBIN_UNITS = myUnique(HEMOGLOBIN_UNITS),
      HEMOGLOBIN_RESULT = myUnique(HEMOGLOBIN_RESULT),
      HEMOGLOBIN_RESULT_DESC = myUnique(HEMOGLOBIN_RESULT_DESC),
      HEMOLYSIS_SCALE = myUnique(HEMOLYSIS_SCALE)
    ) %>%
    full_join(parents %>%
                filter(COLLECTION_GROUP == "WBLD")) %>%
    ungroup() %>%
    mutate(SAMPLE_ID = ifelse(is.na(SAMPLE_ID), PARENT_SAMPLE_ID, SAMPLE_ID)) %>%
    mutate(SPECIMEN_QUANTITY = ifelse(is.na(SPECIMEN_QUANTITY), PARENT_QUANTITY, SPECIMEN_QUANTITY)) %>%
    mutate(COUNT = ifelse(is.na(COUNT), PARENT_COUNT, COUNT)) %>%
    mutate(TYPE = "Blood") %>%
    mutate(BLOOD_PARENTS = ifelse(is.na(PARENTS), "N", "Y")) %>%
    mutate(SPECIMEN_QUANTITY = ifelse(SPECIMEN_QUANTITY == "", 1000, SPECIMEN_QUANTITY)) %>%
    mutate(COUNT = ifelse(is.na(COUNT), 0, COUNT)) %>%
    mutate(QUANTITY_UNITS = "ul") %>%
    ungroup() %>%
    select(SAMPLE_ID, STUDY, DOMAIN, TYPE, VISIT, PDBPStudyID, ALTERNATE_MRN, SPECIMEN_QUANTITY, QUANTITY_UNITS, COUNT,
           PARENTS = BLOOD_PARENTS, HEMOGLOBIN_ASSAY, HEMOGLOBIN_UNITS, HEMOGLOBIN_RESULT, HEMOGLOBIN_RESULT_DESC,
           HEMOLYSIS_SCALE, CLOTTED, TURBIDITY_GRADE)

  if(verbose) message("Building blood pellet specimens catalog...")
  blood_pellet_catalog <- aliquots %>%
    filter(COLLECTION_GROUP == "RBC-WBC" & TYPE == "RBC-WBC") %>%
    group_by(ORIGINAL_PARENT, STUDY, DOMAIN, COLLECTION_GROUP, VISIT, PDBPStudyID, ALTERNATE_MRN, SPECIMEN_QUANTITY) %>%
    summarize(
      SAMPLE_ID = paste0(SAMPLE_ID[!is.na(SAMPLE_ID) & SAMPLE_ID != ""][1], "_", SPECIMEN_QUANTITY),
      COUNT = sum(COUNT),
      HEMOGLOBIN_ASSAY = myUnique(HEMOGLOBIN_ASSAY, summary_function = max, na.rm = TRUE),
      CLOTTED = myUnique(CLOTTED),
      TURBIDITY_GRADE = myMode(TURBIDITY_GRADE),
      TYPE = unique(TYPE),
      HEMOGLOBIN_UNITS = myUnique(HEMOGLOBIN_UNITS),
      HEMOGLOBIN_RESULT = myUnique(HEMOGLOBIN_RESULT),
      HEMOGLOBIN_RESULT_DESC = myUnique(HEMOGLOBIN_RESULT_DESC),
      HEMOLYSIS_SCALE = myUnique(HEMOLYSIS_SCALE)
    ) %>%
    full_join(parents %>%
                filter(COLLECTION_GROUP == "RBC-WBC")) %>%
    ungroup() %>%
    mutate(SAMPLE_ID = ifelse(is.na(SAMPLE_ID), PARENT_SAMPLE_ID, SAMPLE_ID)) %>%
    mutate(SPECIMEN_QUANTITY = ifelse(is.na(SPECIMEN_QUANTITY), PARENT_QUANTITY, SPECIMEN_QUANTITY)) %>%
    mutate(COUNT = ifelse(is.na(COUNT), PARENT_COUNT, COUNT)) %>%
    mutate(TYPE = "Blood Pellet") %>%
    mutate(BLOOD_PARENTS = ifelse(is.na(PARENTS), "N", "Y")) %>%
    mutate(SPECIMEN_QUANTITY = ifelse(SPECIMEN_QUANTITY == "", 1000, SPECIMEN_QUANTITY)) %>%
    mutate(COUNT = ifelse(is.na(COUNT), 0, COUNT)) %>%
    mutate(QUANTITY_UNITS = "ul") %>%
    ungroup() %>%
    select(SAMPLE_ID, STUDY, DOMAIN, TYPE, VISIT, PDBPStudyID, ALTERNATE_MRN, SPECIMEN_QUANTITY, QUANTITY_UNITS, COUNT,
           PARENTS = BLOOD_PARENTS, HEMOGLOBIN_ASSAY, HEMOGLOBIN_UNITS, HEMOGLOBIN_RESULT, HEMOGLOBIN_RESULT_DESC,
           HEMOLYSIS_SCALE, CLOTTED, TURBIDITY_GRADE)

  ref_pools <- createRefPoolCatalogRecords("PDBP", verbose = verbose, env = env)

  if(verbose) message("Joining catalogs...")
  catalog <- plyr::rbind.fill(rna_catalog, dna_catalog, other_catalog, blood_catalog, blood_pellet_catalog, urine_catalog, saliva_catalog, ref_pools) %>%
    mutate(COUNT = ifelse(is.na(COUNT), 0, COUNT)) %>%
    mutate_each(funs(ifelse(is.na(.), "", .))) %>%
    mutate(SPECIMEN_QUANTITY = ifelse(SPECIMEN_QUANTITY != "", SPECIMEN_QUANTITY, MASS)) %>%
    mutate(QUANTITY_UNITS = ifelse(MASS != "", "ug", "ul")) %>%
    mutate(QUANTITY_UNITS = ifelse(SPECIMEN_QUANTITY == "", "", QUANTITY_UNITS)) %>%
    mutate(SAMPLE_ID = ifelse(SAMPLE_ID == "", SAMPLE_ID2, SAMPLE_ID)) %>%
    mutate(Inventory_BiorepositoryDate = format(Sys.time(), format = "%Y-%m-%d %H:%M:%S")) %>%
    filter(!(TYPE == "")) %>%
    filter(!grepl("|", HEMOGLOBIN_ASSAY, fixed = TRUE)) %>%
    filter(!grepl("|", HEMOGLOBIN_RESULT_DESC, fixed = TRUE)) %>%
    select(SAMPLE_ID, STUDY, TYPE, VISIT, PDBPStudyID, ALTERNATE_MRN, SPECIMEN_QUANTITY, QUANTITY_UNITS, COUNT, PARENTS,
           CONCENTRATION, CONCENTRATION_UOM, X260.280_RATIO, X260.230_RATIO, RIN_VALUE, rRatio, QC_CLASS,
           CLOTTED, TURBIDITY_GRADE, HEMOGLOBIN_ASSAY, HEMOGLOBIN_UNITS, HEMOGLOBIN_RESULT, HEMOGLOBIN_RESULT_DESC,
           HEMOLYSIS_SCALE, Inventory_BiorepositoryDate, STUDY) %>%
    arrange(STUDY, ALTERNATE_MRN, VISIT, TYPE) %>%
    distinct()

  #Hacky way to get around duplicate sample ids
#  duplicated_sample_ids <- catalog$SAMPLE_ID[duplicated(catalog$SAMPLE_ID)]
#  catalog2 <- catalog[!catalog$SAMPLE_ID %in% duplicated_sample_ids, ]
   catalog$SAMPLE_ID[duplicated(catalog$SAMPLE_ID)] <- paste0(catalog$SAMPLE_ID[duplicated(catalog$SAMPLE_ID)], ".b")
   suffix <- letters[-c(1)]
   while(sum(duplicated(catalog$SAMPLE_ID)) > 0) {
     catalog$SAMPLE_ID[duplicated(catalog$SAMPLE_ID)] <- gsub(paste0(suffix[1], "$"), suffix[2], catalog$SAMPLE_ID[duplicated(catalog$SAMPLE_ID)])
     suffix <- suffix[-1]
   }

  colnames(catalog) <- c("SampleId", "RepositoryName", "SampCollTyp", "VisitTypPDBP", "PDBPStudyID",
                "GUID", "SampleUnitNum", "SampleUnitMeasurement", "Inventory_BiorepositoryCount",
                "ADDTL_STOCK_AVAIL_ON_REQ", "ConcentrationDNA_RNA", "ConcentrationDNA_RNAUnits",
                "Sample260_280Ratio", "Sample260_230Ratio",
                "RNAIntegrityNum", "rRatio", "RNAQualityScale", "SampleClottingInd",
                "SampleTurbidityScale", "SampleAvgHemoglobinVal",
                "SampleAvgHemoglobinUnits", "SampleAvgHemoglobinResult",
                "SampleAvgHemoglobinResultDesc", "SampleHemolysisScale", "Inventory_BiorepositoryDate")

  catalog[is.na(catalog)] <- ""

  if(verbose) message("Catalog Complete.")
  return(catalog)

}

fixDataForCatalog <- function(oncore_production_data,
                              staging_data_location = "/media/HG-Docs/NINDS Biorepository/CatalogSource/staging_data_for_catalog.csv"){

  staged_data <- read.csv(staging_data_location, stringsAsFactors = FALSE)

  for(i in names(oncore_production_data)){
    col_class <- class(oncore_production_data[[i]])
    if(col_class == "character"){
      staged_data[[i]] <- as.character(staged_data[[i]])
    }else if(col_class == "numeric"){
      staged_data[[i]] <- as.numeric(staged_data[[i]])
    }
  }

  oncore_production_data <- anti_join(oncore_production_data, staged_data,
                                      by = "SPECIMEN_BAR_CODE")

  staged_data <- anti_join(staged_data, oncore_production_data,
                           by = "SPECIMEN_BAR_CODE")

  x <- anti_join(query, oncore_production_data, by = "SPECIMEN_BAR_CODE")

  fixed_data <- rbind(oncore_production_data, staged_data)

  return(fixed_data)
}
