% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/cfiCatalog.R
\name{cfiCatalog}
\alias{cfiCatalog}
\title{biosendCatalog}
\usage{
cfiCatalog(
  protocolName = "MMGE-NINDS-BIOSEND-CFI",
  verbose = interactive(),
  headers = TRUE,
  QAed = TRUE,
  fix = FALSE,
  env = "CLN"
)
}
\arguments{
\item{protocolName}{The protocol to filter on. Used as a grep on the PROTOCOL_NO field.}

\item{verbose}{Should the function let you know what is going on?}
}
\description{
Generate a BioSEND-style catalog
}
\details{
Use this function to generate a catalog of the type suitable for uploading to the BioSEND API.
}
